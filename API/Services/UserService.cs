using System.Collections.Generic;
using System.Linq;
using API.Models;

namespace API.Services
{
    public static class UserService
    {
        static List<User> Users { get; }
        static int nextId = 3;
        static UserService()
        {
            Users = new List<User>
            {
                new User { Id = 1, Name = "pepito", Password = "aaa" },
                new User { Id = 2, Name = "juanito", Password = "123" }
            };
        }

        public static List<User> GetAll() => Users;

        public static User Get(int id) => Users.FirstOrDefault(p => p.Id == id);

        public static void Add(User user)
        {
            user.Id = nextId++;
            Users.Add(user);
        }

        public static void Delete(int id)
        {
            var pizza = Get(id);
            if(pizza is null)
                return;

            Users.Remove(pizza);
        }

        public static void Update(User user)
        {
            var index = Users.FindIndex(p => p.Id == user.Id);
            if(index == -1)
                return;

            Users[index] = user;
        }
    }
}